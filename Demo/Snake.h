#pragma once
#include <graphics.h>
#include <conio.h>
#include "Point.h"

enum direction {UP = 0, DOWN, LEFT, RIGHT};

//蛇体(蛇的最小单元25*25)
class Snake
{
//属性
private:

	//已摄入食物量
	int S_aqfood;

	//方向
	//除却蛇头，其余部分的方向均与前一部分相同
	int S_direction;

	//填充颜色
	//除却蛇头，其余部分均为白色
	COLORREF S_color;

	//坐标
	//蛇体单元的中心坐标 
	Point S_point;


//方法
public:

	//基本的构造函数
	Snake();

	//带基本参数的构造函数
	Snake(int dir, COLORREF color, Point point);

	//吃到食物，蛇体增长
	void Add_S_body();

	//设置方向
	void set_direction(int dir);

	//设置坐标
	void set_point(Point point);

	//获得颜色
	COLORREF get_color();

	//获得当前已吃下的食物数量
	int get_aqfood();

	//食物数量加一
	void add_aqfood();
	
	//获得方向
	int get_direction();
	
	//获得坐标
	Point get_point();

	//虚拟的析构函数
	virtual ~Snake();
};

