#pragma once
#include <graphics.h>
#include <conio.h>
#include <string>
#include "Point.h"
#include "Snake.h"
#include <windows.h>
#include <list>
#include<time.h>



//初始蛇头的位置
#define Start_Point_X 280
#define Start_Point_Y 280

//20*20的格子
#define Line_Background 20

//桌面的大小
#define Back_Ground_Width 780
#define Back_Ground_Height 590

#define Arena_LEFT 20
#define Arena_UP 20
#define Arena_RIGHT 580
#define Arena_DOWN 580

//背景初始化
int Back_Ground_init(void);

//结束界面
void Quit_Guound(void);

//画方框
void draw_box(Point point);

//清除方框
void delete_box(Point point);

