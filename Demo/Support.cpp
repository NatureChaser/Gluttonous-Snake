#include "Support.h"


//背景设计
int Back_Ground_init()
{
	//用作选择模式
	bool flag = true;

	//用作是否正式进入游戏
	bool formal_flag = true;

	//用作选择为人工/AI
	bool modle_flag = true;

	//创建背景桌面
	initgraph(Back_Ground_Width, Back_Ground_Height);

	// 设置当前字体为高 16 像素的“宋体”。(VC6 / VC2008 / VC2010 / VC2012)
	settextstyle(64, 0, _T("宋体"));
	//outtextxy(0, 0, _T("测试"));

	RECT rA = { 50, 50, Back_Ground_Width - 50, 250 };
	drawtext(_T("Gluttonous Snake"), &rA, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	settextstyle(24, 0, _T("宋体"));

	RECT rB = { 50, 330, Back_Ground_Width - 50, 380 };

	drawtext(_T("Person Play"), &rB, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	RECT rC = { 50, 380, Back_Ground_Width - 50, 430 };
	drawtext(_T("AI     Play"), &rC, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	int read_key;

	while (1)
	{
		if (_kbhit())
		{
			read_key = _getch();
			switch (read_key)
			{
				//上
			case 87:
				flag = !flag;
				break;
				//下
			case 83:
				flag = !flag;
				break;

				//回车，进入游戏
			case 13:
				formal_flag = false;
				break;
			default:
				break;
			}
		}
		setfillcolor(YELLOW);

		if (flag)
		{
			clearcircle(300, 405, 6);
			fillcircle(300, 355, 5);
			//人工模式
			modle_flag = false;
		}
		else
		{
			clearcircle(300, 355, 6);
			fillcircle(300, 405, 5);
			//AI模式
			modle_flag = true;
		}

		if (!formal_flag)
		{
			break;
		}

	}
	clearrectangle(0, 0, Back_Ground_Width, Back_Ground_Height);
	settextstyle(18, 0, _T("宋体"));

	line(Arena_LEFT, 0, Arena_LEFT, Back_Ground_Height);
	line(Arena_RIGHT, 0, Arena_RIGHT, Back_Ground_Height);
	line(0, Arena_UP, Arena_RIGHT, Arena_UP);
	line(0, Arena_DOWN, Arena_RIGHT, Arena_DOWN);


	//水平  垂直   居中    单行输出
	//显示游戏相关信息
	RECT r1 = { 600, 50, 770, 100 };

	if (!modle_flag)
	{
		drawtext(_T("当前模式：手动操作"), &r1, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	}
	else
	{
		drawtext(_T("当前模式：AI  模式"), &r1, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	}


	//操作按键
	RECT r2 = { 600, 100, 770, 150 };
	drawtext(_T("W ---- ↑"), &r2, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	RECT r3 = { 600, 150, 770, 200 };
	drawtext(_T("A ---- ←"), &r3, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	RECT r4 = { 600, 200, 770, 250 };
	drawtext(_T("S ---- ↓"), &r4, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	RECT r5 = { 600, 250, 770, 300 };
	drawtext(_T("D ---- →"), &r5, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	//退出
	RECT r6 = { 600, 300, 770, 350 };
	drawtext(_T("ESC ---- Quit"), &r6, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	//暂停/继续
	RECT r7 = { 600, 350, 770, 400 };
	drawtext(_T("Skip ---- ||/>"), &r7, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	//modle_flag == false -------- 人工
	//modle_flag == true  -------- AI
	return modle_flag;
}

//结束界面
void Quit_Guound(void)
{
	clearrectangle(0, 0, Back_Ground_Width, Back_Ground_Height);

	settextstyle(72, 0, _T("宋体"));

	RECT rD = { 50, 100, Back_Ground_Width - 50, 300 };
	drawtext(_T("Game Over"), &rD, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	settextstyle(24, 0, _T("宋体"));

	RECT rE = { 50, 360, Back_Ground_Width - 50, 410 };
	drawtext(_T("Quit"), &rE, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	setfillcolor(YELLOW);
	fillcircle(340, 385, 5);

	int end_all;

	do{
		end_all = _getch();
	} while (end_all != 13); 

	closegraph();
}


//画方框
void draw_box(Point point)
{
	fillrectangle(point.get_x() - 10, point.get_y() - 10, point.get_x() + 10, point.get_y() + 10);
}


//清除方框
void delete_box(Point point)
{
	clearrectangle(point.get_x() - 10, point.get_y() - 10, point.get_x() + 10, point.get_y() + 10);
}

