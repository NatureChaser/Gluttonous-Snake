#pragma once

//基本点
class Point
{
private:

	//坐标
	int x;
	int y;

	//权重
	int weight;

public:
	Point();
	Point(int x, int y);
	Point(int x, int y, int weight);



	Point up_point(void);
	Point down_point(void);
	Point left_point(void);
	Point right_point(void);

	void set_x(int x);
	void set_y(int y);
	void set_w(int w);

	int get_x();
	int get_y();
	int get_w();

	Point operator+(const int& b);

	//重载运算符
	bool operator==(Point b);

	~Point();
};

