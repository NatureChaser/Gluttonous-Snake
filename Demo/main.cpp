#include "Support.h"


using namespace std;

//基本变量声明
/*************************************************************************/
//蛇体链表
list<Snake> Big_Snake;
//正向迭代器1
list<Snake>::iterator it;
//正向迭代器2
list<Snake>::iterator itt;
//反向迭代器A
list<Snake>::reverse_iterator reitA;
//反向迭代器B
list<Snake>::reverse_iterator reitB;


////Point的一个链表
////用于存放所有的背景坐标点
//list<Point> Alist;
////正向迭代器
//list<Point>::iterator Ait;
////反向迭代器
//list<Point>::reverse_iterator Areit;

//暂停/继续标志
bool go = TRUE;

//推出快捷键
bool quit = FALSE;

//当前食物的位置
Point food_current;


//简单的函数声明
/*************************************************************************/
//初始化蛇体链表和Point链表
void init_list();

//动态刷新
void refresh(list<Snake> * able);

//随机食物的位置设定
Point food_set(list<Snake> * able);

//随机食物的生成
void food_create(Point point);

//随即食物的消失
void food_delete(Point point);

//爬
void move_snake(list<Snake> * able);

//画蛇
void draw_snake(list<Snake> * able);

//控制蛇
void control_snake_Pe(list<Snake> *able , int &key);

//控制蛇
void control_snake_AI(list<Snake> *able, Point point, int read_key);

//吃掉食物
void ate_food(list<Snake> * able, Point point);

//蛇长长
void grow_snake(list<Snake> * able, int length);

//返回绝对值
int get_abs(int sample);

//判断死亡
bool judge_dead();

//蛇头碰到边界
bool judge_line_dead(list<Snake> * able);

//蛇头碰到蛇身
bool judge_self_dead(list<Snake> * able);

//读取按键按下
//void read_key_press(int modle);

//基于A*的算法本质
void planning_algorithms(list<Snake> * able, Point point);

//获得当前点的坐标
Point get_new_Point(int x, int y);

//获得权重
int get_p_weight(Point point1, Point point2);

/*************************************************************************/



int main()
{
	//背景初始化
	//模式信号
	int modle_signal = Back_Ground_init();

	//初始化Point和Snake链表
	init_list();

	//产生随机食物
	food_current = food_set(&Big_Snake);

	while (!quit)
	{
		//画蛇
		draw_snake(&Big_Snake);

		//人工模式
		if (!modle_signal)
		{
			if (_kbhit())
			{
				int read_key = _getch();
				control_snake_Pe(&Big_Snake, read_key);
			}
		}
		//AI模式
		else
		{
			if (_kbhit())
			{
				int read_key = _getch();
				switch (read_key)
				{
					//空格--暂停/继续
				case 32:
					go = !go;
					break;
					//ESC--退出
				case 27:
					go = FALSE;
					quit = !quit;
					break;
				default:
					break;
				}
			}
			//AI操控
			planning_algorithms(&Big_Snake, food_current);
		}

		//画食物
		food_create(food_current);

		if (judge_dead())
		{
			break;
		}

		if (go)
		{
			//吃
			ate_food(&Big_Snake, food_current);
			//反复刷新
			refresh(&Big_Snake);
			//爬
			move_snake(&Big_Snake);
		}
	}
	Quit_Guound();
	return 0;
}




/*************************************************************************/


//画蛇
//box：20*20
void draw_snake(list<Snake> * able)
{
	
	
	////画蛇头
	//setfillcolor((*it).get_color());
	//draw_box((*it).get_point());
	//it++;

	
	for (it = (*able).begin(); it != (*able).end(); it++)
	{
		setfillcolor((*it).get_color());
		//画方框
		draw_box((*it).get_point());
	}
}


void refresh(list<Snake> * able)
{
	//draw_snake(able);
	Sleep(200);

	it = (*able).begin();

	for (; it != (*able).end(); it++)
	{
		//清除方框
		delete_box((*it).get_point());
	}
	//Sleep(10);
}

//爬
//从后往前遍历
void move_snake(list<Snake> * able)
{
	reitA = (*able).rbegin();
	reitB = (*able).rbegin();
	reitB++;
	//更换身体部分的坐标
	while (reitB != (*(able)).rend())
	{
		(*reitA).set_point((*(reitB)).get_point());
		reitA++;
		reitB++;
	}

	it = (*able).begin();

	//更换头的坐标
	switch ((*it).get_direction())
	{
	case UP:
		(*it).set_point((*it).get_point().up_point());
		break;
	case DOWN:
		(*it).set_point((*it).get_point().down_point());
		break;
	case LEFT:
		(*it).set_point((*it).get_point().left_point());
		break;
	case RIGHT:
		(*it).set_point((*it).get_point().right_point());
		break;
	default:
		//undo
		break;
	}
}

//控制蛇
void control_snake_Pe(list<Snake> *able, int &key)
{
	it = (*able).begin();
	switch (key)
	{
		//均为大写字母
		//W--向上
	case 87:
		if ((*it).get_direction() != DOWN)
		{
			(*it).set_direction(UP);
		}
		break;
		//A--向左
	case 65:
		if ((*it).get_direction() != RIGHT)
		{
			(*it).set_direction(LEFT);
		}
		break;
		//S--向下
	case 83:
		if ((*it).get_direction() != UP)
		{
			(*it).set_direction(DOWN);
		}
		break;
		//D--向右
	case 68:
		if ((*it).get_direction() != LEFT)
		{
			(*it).set_direction(RIGHT);
		}
		break;
		//空格--暂停/继续
	case 32:
		go = !go;
		break;
		//ESC--退出
	case 27:
		go = FALSE;
		quit = !quit;
		break;
	default:
		break;
	}
}

//控制蛇
void control_snake_AI(list<Snake> *able, Point point, int read_key)
{
	planning_algorithms(able, point);

	switch (read_key)
	{
		//空格--暂停/继续
	case 32:
		go = !go;
		break;
		//ESC--退出
	case 27:
		go = FALSE;
		quit = !quit;
		break;
	default:
		break;
	}
}


//随机食物的生成
Point food_set(list<Snake> * able)
{
	bool flag_again;
	int point_x;
	int point_y;
	Point point;


	srand(time(NULL));

	while (1)
	{
		//重复的标志
		flag_again = false;

		point_y = rand() % 27;
		point_x = rand() % 27;

		point.set_x(point_x * 20 + 40);
		point.set_y(point_y * 20 + 40);

		for (it = (*able).begin(); it != (*able).end(); it++)
		{
			if (point == (*it).get_point())
			{
				flag_again = true;
				break;
			}
		}
		if (flag_again == false)
		{
			break;
		}
	}
	return point;
}


//随即食物的生成
void food_create(Point point)
{
	setfillcolor(GREEN);
	draw_box(point);
	//fillrectangle(point.get_x() * 20, point.get_y() * 20, (point.get_x() + 1) * 20, (point.get_y() + 1) * 20);
}


//随即食物的消失
void food_delete(Point point)
{
	delete_box(point);
}

//吃掉食物
void ate_food(list<Snake> * able, Point point)
{
	it = (*able).begin();
	if ((*it).get_point() == point)
	{
		//已吃食物+1
		(*it).add_aqfood();

		//食物消失
		food_delete(food_current);

		//蛇长长一格
		grow_snake(able, 1);

		//新食物诞生
		food_current = food_set(&Big_Snake);
	}
}

//蛇长长
//长长的部分叠加在原来的上面
//等等长数次循环后
//显现
void grow_snake(list<Snake> * able, int length)
{
	Point current_point;
	int current_dir;
	it = (*able).end();
	it--;

	for (int i = 0; i < length; i++)
	{
		current_point = (*it).get_point();
		current_dir = (*it).get_direction();
		Big_Snake.push_back(Snake( current_dir, RED, current_point ));
		it++;
	}
}


//判断死亡
bool judge_dead()
{
	if (judge_line_dead(&Big_Snake) || judge_self_dead(&Big_Snake))
	{
		return true;
	}
	return false;
}

//蛇头碰到边界
bool judge_line_dead(list<Snake> * able)
{
	for (it = (*able).begin(); it != (*able).end(); it++)
	{
		if ((*it).get_point().get_x() == Arena_LEFT ||
			(*it).get_point().get_x() == Arena_RIGHT ||
			(*it).get_point().get_y() == Arena_UP ||
			(*it).get_point().get_y() == Arena_DOWN)
		{
			return true;
			break;
		}
	}
	return false;
}

//蛇头碰到蛇身
bool judge_self_dead(list<Snake> * able)
{
	itt = (*able).begin();
	it = (*able).begin();
	it++;
	for (; it != (*able).end(); it++)
	{
		if ((*itt).get_point() == (*it).get_point())
		{
			return true;
			break;
		}
	}
	return false;
}

//读取按键按下
//void read_key_press(int modle)
//{
//	//读取键盘事件
//	if (_kbhit())
//	{
//		int read_key = _getch();
//		if (!modle)
//		{
//			//手动操作
//			control_snake_Pe(&Big_Snake, read_key);
//		}
//		else
//		{
//			//AI操控
//			control_snake_AI(&Big_Snake, food_current, read_key);
//		}
//	}
//}


//算法设计思路
//利用局部最优解来实现
//贪心就完事了

//路径规划算法本质
void planning_algorithms(list<Snake> * able,  Point point)
{
	it = (*able).begin();

	//蛇可能走的四个格子
	Point current_point[4];

	//上面
	current_point[0].set_x((*it).get_point().get_x());
	current_point[0].set_y((*it).get_point().get_y() - 20);

	//line((*it).get_point().get_x(), 0, (*it).get_point().get_x(), 590);
	//line(0, (*it).get_point().get_y(), 720, (*it).get_point().get_y());

	//下面
	current_point[1].set_x((*it).get_point().get_x());
	current_point[1].set_y((*it).get_point().get_y() + 20);

	//左面
	current_point[2].set_x((*it).get_point().get_x() - 20);
	current_point[2].set_y((*it).get_point().get_y());

	//右面
	current_point[3].set_x((*it).get_point().get_x() + 20);
	current_point[3].set_y((*it).get_point().get_y());

	//给予权重
	for (int i = 0; i < 4; i++)
	{
		current_point[i].set_w(get_p_weight(point, current_point[i]));
	}

	for (it = (*able).begin(); it != (*able).end(); it++)
	{
		for (int i = 0; i < 4; i++)
		{
			if (current_point[i].get_x() == 20 || current_point[i].get_x() == 580 || current_point[i].get_y() == 20 || current_point[i].get_y() == 580)
			{
				current_point[i].set_w(10000);
			}

			if (current_point[i] == (*it).get_point())
			{
				current_point[i].set_w(10000);
			}
		}
	}

	Point a = current_point[0];

	int b = 0;

	for (int i = 1; i < 4; i++)
	{
		if (a.get_w() > current_point[i].get_w())
		{
			a = current_point[i];
			b = i;
		}
	}
	
	it = (*able).begin();

	switch (b)
	{
	case 0:
		(*it).set_direction(UP);
		break;
	case 1:
		(*it).set_direction(DOWN);
		break;
	case 2:
		(*it).set_direction(LEFT);
		break;
	case 3:
		(*it).set_direction(RIGHT);
		break;
	default:
		break;
	}
	//判断头和食物之间是否存在障碍

	//如果没有，则莽过去

	//如果存在，则往最深的路径走
}


//获得当前点的坐标
Point get_new_Point(int x, int y)
{
	Point point(40 + 20 * x, 40 + 20 * y);

	return point;
}


//初始化蛇体链表和Point链表
void init_list()
{
	//new 完之后是个指针
	Snake snake_head = Snake(LEFT, YELLOW, Point(Start_Point_X, Start_Point_Y));

	//刚开始最好是两节，或者加标志给碰撞判断
	Snake snake_next = Snake(LEFT, RED, Point(Start_Point_X + 20, Start_Point_Y));

	Big_Snake.push_back(snake_head);

	Big_Snake.push_back(snake_next);

	//初始化，5节蛇体
	grow_snake(&Big_Snake, 4);
}


//获得权重
int get_p_weight(Point point1, Point point2)
{
	int a1 = get_abs(point1.get_x() - point2.get_x());

	int a2 = get_abs(point1.get_y() - point2.get_y());

	return ((a1 + a2) / 20 );
}

//得到绝对值
int get_abs(int sample)
{
	if (sample < 0)
	{
		return (-1 * sample);
	}
	return sample;
}
