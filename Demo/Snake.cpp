#include "Snake.h"



//基本的构造函数
Snake::Snake()
{
	this->S_aqfood = 0;
	this->S_direction = LEFT;
	this->S_color = WHITE;
	this->S_point.set_x(50);
	this->S_point.set_y(60);
}

//带基本参数的构造函数
Snake::Snake(int dir, COLORREF color, Point point)
{
	this->S_aqfood = 0;
	this->S_color = color;
	this->S_direction = dir;
	this->S_point = point;
}


//设置方向
void Snake::set_direction(int dir)
{
	this->S_direction = dir;
}


//设置坐标
void Snake::set_point(Point point)
{
	this->S_point = point;
}

//吃到食物，蛇体增长
void Snake::Add_S_body()
{

}


//获得颜色
COLORREF Snake::get_color()
{
	return this->S_color;
}

//获得方向
int Snake::get_direction()
{
	return this->S_direction;
}

//获得坐标
Point Snake::get_point()
{
	return this->S_point;
}



//虚拟的析构函数
Snake:: ~Snake()
{

}


//获得当前已吃下的食物数量
int Snake::get_aqfood()
{
	return this->S_aqfood;
}

//食物数量加一
void Snake::add_aqfood()
{
	this->S_aqfood = this->S_aqfood + 1;
}