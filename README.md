# Gluttonous Snake

#### 介紹
基于easyX绘图插件以及list容器的cpp AI智障贪吃蛇

#### 架構


![整体流程](https://images.gitee.com/uploads/images/2019/1026/200408_6b44325c_5395228.png "all.png")
![ai流程](https://images.gitee.com/uploads/images/2019/1026/200458_9a3ff4a6_5395228.png "AI.png")
![手动流程](https://images.gitee.com/uploads/images/2019/1026/200510_52ebe714_5395228.png "person.png")


#### 使用說明

*  前往easyx官网[官网链接](https://easyx.cn/)下载并安装
*  直接编译运行main.cpp


### 运行效果
![开始界面](https://images.gitee.com/uploads/images/2019/1026/200524_bba2660b_5395228.png "START.png")
![运行效果](https://images.gitee.com/uploads/images/2019/1026/200538_e7210595_5395228.png "ING.png")
![游戏结束](https://images.gitee.com/uploads/images/2019/1026/200549_7d24bc3f_5395228.png "OVER.png")

### 提醒

* 下载使用务必 请 声明出处 并 附上本文链接 https://gitee.com/NatureChaser/Gluttonous-Snake （谢谢合作）

#### 參與貢獻

```
NatureChaser
```

